CREATE TABLE IF NOT EXISTS user_auth (
    id UUID NOT NULL PRIMARY KEY,
    email VARCHAR (255),
    password VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    access_token TEXT,
    refresh_token TEXT,
    is_verified BOOLEAN DEFAULT false,
    is_blocked_by_admin BOOLEAN,
    username VARCHAR(100) UNIQUE
);

CREATE TABLE IF NOT EXISTS user_client (
    user_id UUID PRIMARY KEY,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    username VARCHAR(100) UNIQUE NOT NULL,
    bio VARCHAR(255),
    profile_photo TEXT,
    access_token TEXT NOT NULL,
    refresh_token TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS auth_connections (
    user_id UUID NOT NULL,
    social_id VARCHAR(255),
    access_token TEXT,
    connection_type VARCHAR(255),
    CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES user_client(user_id)
);
