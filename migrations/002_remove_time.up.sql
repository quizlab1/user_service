
ALTER TABLE user_client 
DROP COLUMN IF EXISTS created_at;


ALTER TABLE user_client 
DROP COLUMN IF EXISTS updated_at;


ALTER TABLE user_client 
DROP COLUMN IF EXISTS deleted_at;

ALTER TABLE user_client 
DROP COLUMN IF EXISTS username;

ALTER TABLE user_client 
DROP COLUMN IF EXISTS access_token;

ALTER TABLE user_client 
DROP COLUMN IF EXISTS refresh_token;

alter table user_auth alter column deleted_at drop default;