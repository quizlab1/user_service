package service

import (
	"context"
	"database/sql"
	"net/http"
	"strings"

	gpb "github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	pb "gitlab.com/quizlab/user_service/genproto/user_service"
	"gitlab.com/quizlab/user_service/pkg/logger"
	l "gitlab.com/quizlab/user_service/pkg/logger"
	grpcclient "gitlab.com/quizlab/user_service/service/grpc_client"
	"gitlab.com/quizlab/user_service/storage"
	"gitlab.com/quizlab/user_service/storage/repo"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// UserService ...
type UserService struct {
	storage storage.I
	client  grpcclient.IServiceManager
	logger  l.Logger
}

// NewUserService ...
func NewUserService(db *sqlx.DB, log l.Logger, client grpcclient.IServiceManager) *UserService {
	return &UserService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		client:  client,
	}
}

// CheckField ...
func (s *UserService) CheckField(ctx context.Context, req *pb.CheckFieldRequest) (*pb.CheckFieldResponse, error) {
	resp, err := s.storage.User().CheckField(req.Field, req.Value)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while checking field, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while checking field", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.CheckFieldResponse{
		Exists: resp,
	}, nil
}

// CreateAuthUser is a method for creating a client
func (s *UserService) CreateAuthUser(ctx context.Context, req *pb.User) (*pb.User, error) {

	// _, err := s.client.NotificationService().CreateUserNotificationSettings(
	// 	context.Background(), &notificationPb.CreateUserNotificationSettingRequest{
	// 		UserId: req.GetId(),
	// 	},
	// )
	// if err != nil {
	// 	s.logger.Error("failed to create user notification settings", l.Error(err))
	// 	return nil, status.Error(codes.Internal, "failed to create user notification settings")
	// }

	createdClient, err := s.storage.User().CreateAuthUser(req)
	if err == repo.ErrAlreadyExists {
		s.logger.Error("Error while creating auth user, Already Exists", l.Any("req", req))
		return nil, status.Error(codes.AlreadyExists, "Already Exists")
	} else if err == sql.ErrNoRows {
		s.logger.Error("Error while creating auth user, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while creating auth user", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}
	// var autoFollowID string

	// if os.Getenv("ENVIRONMENT") == "develop" || s.cfg.Environment == "staging" {
	// 	autoFollowID = "fff0e2a7-d356-4aff-87b1-241fb57d9dbb"
	// } else {
	// 	autoFollowID = "7601fa01-606e-42e2-b213-5f91ce89fbd3"
	// }

	// err = s.storage.User().Follow(req.Id, autoFollowID)
	// if err == repo.ErrAlreadyExists {
	// 	s.logger.Error("Error while following, Already Exists", l.Any("req", req))
	// 	return nil, status.Error(codes.AlreadyExists, "Already Exists")
	// } else if err == sql.ErrNoRows {
	// 	s.logger.Error("Error while following, Not Found", l.Any("req", req))
	// 	return nil, status.Error(codes.NotFound, "Not found")
	// } else if err != nil {
	// 	s.logger.Error("Error while following", l.Error(err), l.Any("req", req))
	// 	return nil, status.Error(codes.Internal, "Internal server error")
	// }

	// err = s.storage.User().FollowApprove(req.Id, autoFollowID)
	// if err == repo.ErrAlreadyExists {
	// 	s.logger.Error("Error while approve following, Already Exists", l.Any("req", req))
	// 	return nil, status.Error(codes.AlreadyExists, "Already Exists")
	// } else if err == sql.ErrNoRows {
	// 	s.logger.Error("Error while approve following, Not Found", l.Any("req", req))
	// 	return nil, status.Error(codes.NotFound, "Not found")
	// } else if err != nil {
	// 	s.logger.Error("Error while approve following", l.Error(err), l.Any("req", req))
	// 	return nil, status.Error(codes.Internal, "Internal server error")
	// }

	return createdClient, nil
}

// GetAuthUser is a method for getting one client
func (s *UserService) GetAuthUser(ctx context.Context, req *pb.GetAuthUserReq) (*pb.User, error) {
	var user *pb.User

	user, err := s.storage.User().GetAuthUser(req.Id)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting auth user, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting auth user", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return user, nil
}

// GetByEmail ...
func (s *UserService) GetByEmail(ctx context.Context, req *pb.GetByEmailReq) (*pb.GetByEmailResp, error) {
	var user *pb.User

	user, err := s.storage.User().GetByEmail(req.Email)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting auth user by email, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting auth user by email", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetByEmailResp{User: user}, nil
}

// UpdateUserTokens ...
func (s *UserService) UpdateUserTokens(ctx context.Context, req *pb.UpdateUserTokensReq) (*gpb.Empty, error) {
	err := s.storage.User().UpdateUserTokens(req.Id, req.AccessToken, req.RefreshToken)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while updating user tokens, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while updating user tokens", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &gpb.Empty{}, nil
}

//SetFcmToken ...
func (s *UserService) SetFcmToken(ctx context.Context, req *pb.SetFcmTokenRequest) (*gpb.Empty, error) {
	err := s.storage.User().SetFcmToken(req.Id, req.FcmToken)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while setting fcm token, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while setting fcm token", l.Error(err), l.Any("req", req))

		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &gpb.Empty{}, nil
}

// Login is a method for following a user
func (s *UserService) Login(ctx context.Context, req *pb.LoginReq) (*pb.LoginResp, error) {
	req.Email = strings.ToLower(req.Email)
	req.Email = strings.TrimSpace(req.Email)

	user, err := s.storage.User().GetByEmail(req.Email)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting user by email, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting user by email", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password))
	if err != nil {
		s.logger.Error("Error while comparing hashed password, Invlid credentials", l.Any("req", req))
		return nil, status.Error(codes.InvalidArgument, "Invlid credentials")
	}

	return &pb.LoginResp{
		User: user,
	}, nil
}

// Logout ...
func (s *UserService) Logout(ctx context.Context, req *pb.LogoutRequest) (*gpb.Empty, error) {
	err := s.storage.User().Logout(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while loging out, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not Found")
	} else if err != nil {
		s.logger.Error("Error while loging out", l.Error(err), l.Any("req", req))
		return nil, status.Error(http.StatusBadRequest, "Not Found")
	}

	return &gpb.Empty{}, nil
}

// SetPassword is a method for setting user password
func (s *UserService) SetPassword(ctx context.Context, req *pb.SetPasswordReq) (*gpb.Empty, error) {
	err := s.storage.User().SetPassword(req.Id, req.Password)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while setting user password, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while setting user password", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &gpb.Empty{}, nil
}

// GetTotalScore ...
func (s *UserService) GetTotalScore(ctx context.Context, req *pb.GetTotalScoreRequest) (*pb.GetTotalScoreResponse, error) {
	resp, err := s.storage.User().GetTotalScore(req.UserId)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while checking user, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while checking user", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetTotalScoreResponse{
		TotalScore: resp,
	}, nil
}

// SetTotalScore ...
func (s *UserService) SetTotalScore(ctx context.Context, req *pb.SetTotalScoreRequest) (*gpb.Empty, error) {
	err := s.storage.User().SetTotalScore(req.GetUserId(), req.GetTotalScore())
	if err != nil {
		s.logger.Error("failed update note", logger.Error(err))
		return &gpb.Empty{}, status.Error(codes.Internal, "failed to update total score")
	}

	return &gpb.Empty{}, nil
}

// GetUserRating ...
func (s *UserService) GetUserRating(ctx context.Context, req *pb.GetUserRatingRequest) (*pb.GetUserRatingResponse, error) {
	resp, err := s.storage.User().GetUserRating(req.UserId)
	if err == sql.ErrNoRows {
		s.logger.Error("Error while checking user, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while checking user", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetUserRatingResponse{
		Rating: resp,
	}, nil
}

// GetLeadersList is a method for getting leaders list))
func (s *UserService) GetLeadersList(ctx context.Context, req *pb.GetLeadersListRequest) (*pb.GetLeadersListResponse, error) {
	var (
		err      error
		requests []*pb.Leader
		count    uint64
	)

	requests, count, err = s.storage.User().GetLeaderList(req.GetUserId(), req.GetLimit(), req.GetPage())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting leader, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting leader", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetLeadersListResponse{
		Leaders: requests,
		Count: count,
	}, nil
}