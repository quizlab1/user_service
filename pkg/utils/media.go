package utils

import "fmt"

// GetPostImage1000x1000URL ...
func GetPostImage1000x1000URL(mediaID string) string {
	return fmt.Sprintf("https://ucook-storage-resized.s3.us-east-2.amazonaws.com/post_image/1080x1080/%s.jpg", mediaID)
}

// GetAvatar300x300URL ...
func GetAvatar300x300URL(mediaID string) string {
	return fmt.Sprintf("https://ucook-storage-resized.s3.us-east-2.amazonaws.com/avatars/300x300/%s.jpg", mediaID)
}

// GetAvatar500x500URL ...
func GetAvatar500x500URL(mediaID string) string {
	return fmt.Sprintf("https://ucook-storage-resized.s3.us-east-2.amazonaws.com/avatars/500x500/%s.jpg", mediaID)
}

// GetPostVideo720pURL ...
func GetPostVideo720pURL(mediaID string) string {
	return fmt.Sprintf("https://ucook-storage-resized.s3.us-east-2.amazonaws.com/videos/720p/%s.mp4", mediaID)
}

// GetPostVideo720pThumbnailURL ...
func GetPostVideo720pThumbnailURL(mediaID string) string {
	return fmt.Sprintf("https://ucook-storage-resized.s3.us-east-2.amazonaws.com/thumbnails/720x1280/%s.jpg", mediaID)
}
