package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/quizlab/user_service/storage/postgres"
	"gitlab.com/quizlab/user_service/storage/repo"
)

// I is an interface for storage
type I interface {
	User() repo.UserStorageI
}

type storagePg struct {
	db        *sqlx.DB
	userRepo  repo.UserStorageI
}

// NewStoragePg ...
func NewStoragePg(db *sqlx.DB) I {
	return &storagePg{
		db:        db,
		userRepo:  postgres.NewUserRepo(db),
	}
}

func (s storagePg) User() repo.UserStorageI {
	return s.userRepo
}
