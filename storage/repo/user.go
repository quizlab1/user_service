package repo

import (
	"errors"
	pb "gitlab.com/quizlab/user_service/genproto/user_service"
)

var (
	// ErrAlreadyExists ...
	ErrAlreadyExists = errors.New("Already exists")
	// ErrInvalidField ...
	ErrInvalidField = errors.New("Incorrect field")
)

// UserStorageI is an interface for client storage
type UserStorageI interface {
	CheckField(field, value string) (bool, error)
	CreateAuthUser(*pb.User) (*pb.User, error)
	GetAuthUser(id string) (*pb.User, error)
	GetByEmail(email string) (*pb.User, error)
	UpdateUserTokens(id, accessToken, refreshToken string) error
	SetFcmToken(id, fcmToken string) error
	Logout(id string) error
	SetPassword(id, password string) error
	GetTotalScore(userID string) (int64, error)
	SetTotalScore(userID string, totalScore int64) error
	GetUserRating(userID string) (int64, error)
	GetLeaderList(userID string, limit, page uint64) ([]*pb.Leader, uint64, error)
}
