package postgres

import (
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	pb "gitlab.com/quizlab/user_service/genproto/user_service"
	"gitlab.com/quizlab/user_service/pkg/utils"
	"gitlab.com/quizlab/user_service/storage/repo"
)

type userRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewUserRepo(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{
		db: db,
	}
}

func (ur *userRepo) CheckField(field, value string) (bool, error) {
	var existsClient int

	if field == "username" {
		row := ur.db.QueryRow(`
			SELECT count(1) FROM user_auth WHERE username = $1 AND deleted_at IS NULL`, value,
		)

		if err := row.Scan(&existsClient); err != nil {
			return false, err
		}

	} else if field == "email" {
		row := ur.db.QueryRow(`
			SELECT count(1) FROM user_auth WHERE email = $1 AND deleted_at IS NULL`, value,
		)

		if err := row.Scan(&existsClient); err != nil {
			return false, err
		}
	} else {
		return false, repo.ErrInvalidField
	}

	if existsClient == 0 {
		return false, nil
	}

	return true, nil
}

func (ur *userRepo) CreateAuthUser(usr *pb.User) (*pb.User, error) {
	var (
		email        = utils.StringToNullString(usr.Email)
		bio          = utils.StringToNullString(usr.Bio)
		err          error
	)

	tx, err := ur.db.Begin()
	if err != nil {
		return nil, err
	}

	insertClient := `INSERT INTO
	user_auth
	(
		id,
		created_at,
		updated_at,
		email,
		password,
		access_token,
		refresh_token,
		username
	)
		VALUES
		($1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $2, $3, $4, $5, $6)`
	_, err = tx.Exec(
		insertClient,
		usr.Id,
		email,
		usr.Password,
		usr.AccessToken,
		usr.RefreshToken,
		usr.Username,
	)

	if pgerr, ok := err.(*pq.Error); ok {
		if pgerr.Code == "23505" {
			tx.Rollback()
			return nil, repo.ErrAlreadyExists
		}
		return nil, err
	} else if err != nil {
		tx.Rollback()
		return nil, err
	}

	insertNew :=
		`INSERT INTO
		user_client
		(
			id,
			first_name,
			last_name,
			bio,
			profile_photo,
			fcm_token
		)
			VALUES
			($1, $2, $3, $4, $5, $6)`
	_, err = tx.Exec(insertNew,
		usr.Id,
		usr.FirstName,
		usr.LastName,
		bio,
		"https://i.ibb.co/rQkGQgS/avatar.png",
		usr.FcmToken,
	)

	if pgerr, ok := err.(*pq.Error); ok {
		if pgerr.Code == "23505" {
			tx.Rollback()
			return nil, repo.ErrAlreadyExists
		}
		return nil, err
	} else if err != nil {
		tx.Rollback()
		return nil, err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	createdUser, err := ur.GetAuthUser(usr.Id)
	if err != nil {
		return nil, err
	}

	return createdUser, err
}

func (ur *userRepo) GetAuthUser(id string) (*pb.User, error) {
	cli := pb.User{}
	var (
		createdAt, updatedAt time.Time
		bio                  sql.NullString
		profilePhoto         sql.NullString
		email                sql.NullString
		fcmToken             sql.NullString
	)

	row := ur.db.QueryRow(`
		SELECT
		       uc.id,
		       first_name,
		       last_name,
		       profile_photo,
		       fcm_token,
		       bio,
		       created_at,
		       updated_at,
		       access_token,
		       refresh_token,
		       a.email,
		       username
		FROM user_auth a JOIN user_client uc on a.id = uc.id WHERE deleted_at IS NULL AND uc.id = $1`, id)
	err := row.Scan(
		&cli.Id,
		&cli.FirstName,
		&cli.LastName,
		&profilePhoto,
		&fcmToken,
		&bio,
		&createdAt,
		&updatedAt,
		&cli.AccessToken,
		&cli.RefreshToken,
		&email,
		&cli.Username,
	)
	if err != nil {
		return nil, err
	}
	cli.Bio = bio.String
	cli.Email = email.String
	cli.ProfilePhoto = profilePhoto.String
	cli.FcmToken = fcmToken.String
	cli.CreatedAt = createdAt.Format(time.RFC3339)
	cli.UpdatedAt = updatedAt.Format(time.RFC3339)

	return &cli, nil
}

func (ur *userRepo) GetByEmail(email string) (*pb.User, error) {
	cli := pb.User{}
	var (
		createdAt, updatedAt   time.Time
		bio                    sql.NullString
		profilePhoto, fcmToken sql.NullString
	)

	row := ur.db.QueryRow(`
		SELECT
		    a.id,
		    first_name,
			last_name,
			profile_photo,
			fcm_token,
			bio,
       		created_at,
       		updated_at,
       		password,
       		access_token,
       		refresh_token,
			username
		FROM user_auth a JOIN user_client u ON a.id = u.id
		WHERE deleted_at IS NULL AND a.email = $1
	`, email)
	err := row.Scan(
		&cli.Id,
		&cli.FirstName,
		&cli.LastName,
		&profilePhoto,
		&fcmToken,
		&bio,
		&createdAt,
		&updatedAt,
		&cli.Password,
		&cli.AccessToken,
		&cli.RefreshToken,
		&cli.Username,
	)
	if err != nil {
		return nil, err
	}
	cli.Bio = bio.String
	cli.ProfilePhoto = profilePhoto.String
	cli.CreatedAt = createdAt.Format(time.RFC3339)
	cli.UpdatedAt = updatedAt.Format(time.RFC3339)
	cli.FcmToken = fcmToken.String

	return &cli, nil
}

func (ur *userRepo) UpdateUserTokens(id, accessToken, refreshToken string) error {
	updateTokens := `
	UPDATE user_auth
	SET
		access_token = $1,
		refresh_token = $2
	WHERE id = $3
	`
	result, err := ur.db.Exec(updateTokens,
		accessToken,
		refreshToken,
		id,
	)
	if err != nil {
		return err
	}

	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rows == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ur *userRepo) SetFcmToken(id, fcmToken string) error {
	result, err := ur.db.Exec(`
		UPDATE user_client SET fcm_token = $1 WHERE id = $2`, fcmToken, id,
	)
	if err != nil {
		return err
	}

	if i, _ := result.RowsAffected(); i == 0 {
		return sql.ErrNoRows
	}

	return nil
}

// Logout deletes fcm_token of a specific usr_client
func (ur *userRepo) Logout(id string) (err error) {
	statement := `UPDATE user_client SET fcm_token SET NULL WHERE id = $1`

	stmt, err := ur.db.Prepare(statement)
	if err != nil {
		return err
	}

	result, err := stmt.Exec(id)
	if err != nil {
		return err
	}

	if i, _ := result.RowsAffected(); i == 0 {
		return sql.ErrNoRows
	}

	return
}

func (ur *userRepo) SetPassword(id, password string) error {
	result, err := ur.db.Exec(`
		UPDATE user_auth
		SET password = $1
		WHERE id = $2
	`, password, id)
	if err != nil {
		return err
	}

	if i, _ := result.RowsAffected(); i == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ur *userRepo) GetTotalScore(userID string) (int64, error) {
	var totalScore int64

	row := ur.db.QueryRow(`SELECT total_score FROM user_client where
		id=$1`, userID)

	err := row.Scan(&totalScore)
	if err != nil {
		return 0, err
	}

	return totalScore, nil
}

func (ur *userRepo) SetTotalScore(userID string, totalScore int64) error {
	updatetScore := `UPDATE
		user_client SET
			total_score=$1
			WHERE id=$2`

	_, err := ur.db.Exec(
		updatetScore,
		totalScore,
		userID,
	)
	if err != nil {
		return err
	}
	return nil
}

func (ur *userRepo) GetUserRating(userID string) (int64, error) {
	var rating int64

	row := ur.db.QueryRow(`
	SELECT d.rating
	FROM (
			 SELECT id,
			 row_number() OVER (ORDER 
						BY total_score DESC) AS rating
			 FROM user_client
			 ORDER BY total_score DESC, rating
		 ) as d
	WHERE d.id = $1`, userID)

	err := row.Scan(&rating)
	if err != nil {
		return 0, err
	}

	return rating, nil
}

func (ur *userRepo) GetLeaderList(userID string, limit, page uint64) ([]*pb.Leader, uint64, error) {
	var (
		requests      []*pb.Leader
		count, offset uint64
		profilePhoto  sql.NullString
	)

	offset = (page - 1) * limit

	rows, err := ur.db.Query(`
	SELECT u.id,
       u.first_name,
       u.last_name,
       a.username,
       u.profile_photo,
       row_number() over  (ORDER
           BY u.total_score DESC) AS rating,
	   u.total_score
	FROM user_client u JOIN user_auth a on u.id = a.id
	WHERE a.deleted_at IS NULL ORDER BY total_score DESC, rating
	LIMIT $1 OFFSET $2`, limit, offset)

	if err != nil {
		return nil, 0, err
	}

	for rows.Next() {
		var ld pb.Leader
		if err := rows.Scan(
			&ld.Id,
			&ld.FirstName,
			&ld.LastName,
			&ld.Username,
			&profilePhoto,
			&ld.Rating,
			&ld.TotalScore,
		); err != nil {
			return nil, 0, err
		}
		ld.ProfilePhoto = profilePhoto.String
		requests = append(requests, &ld)
	}

	count, err = ur.GetLeadersCount()
	if err != nil {
		return nil, 0, err
	}

	return requests, count, nil
}

func (ur *userRepo) GetLeadersCount() (uint64, error) {
	var count uint64

	row := ur.db.QueryRow(`
 		SELECT count(*)
		FROM user_auth 
		WHERE deleted_at IS NULL 
 	`)

	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}
